# Site du GT maths-bio

Ce dépôt contient les sources du [site du GT maths-bio](http://gt-mathsbio.biologie.ens.fr).
Vous pouvez contribuer de deux manières :
- En demandant à être ajouté à la liste des membres du projet, de manière à
  pouvoir soumettre vos modifications directement (bouton `Request Access` en
  haut à droite de cette page).
- En signalant tout problème sur le [tracker](https://gitlab.com/bienvenu/Site_GT_maths-bio/issues).

Si vous ne souhaitez pas créer de compte GitLab, vous pouvez quand même
contribuer en nous écrivant à l'adresse <gt-mathsbio.contact@lists.ens.fr>.

# Licence

Copyright © 2013-2016 GT maths-bio <gt-mathsbio.contact@lists.ens.fr>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the LICENSE file for more details.
