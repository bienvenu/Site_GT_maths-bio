png("stats_gt_2013.png")

par(bg = "#002B36", fg = "#839496", col = "#839496",
    col.axis ="#839496", col.lab ="#839496", col.main ="#839496",
    pch = 19)

plot(c(), c(), xlim = c(1, 13), xlab = "Séance",
     ylim = c(0, 20), ylab = "Nombre de personnes",
     main = "Participation au GT (2013)")

benjamin = data.frame(x = c(1, 2), y = c(17, 13), col = "#2AA198")

francois = data.frame(x = c(3, 4, 5), y = c(18, 13, 10))

guilhem = data.frame(x = c(6, 7), y = c(16, 14))

alexis = data.frame(x = c(8, 9), y = c(11, 12))

maxime = data.frame(x = c(10, 11), y = c(11, 8))

guillaume = data.frame(x = c(12, 13), y = c(11, 5))

lines(benjamin, col = "#2AA198")
lines(c(2, 3), c(13, 18), lty="dashed")
lines(francois, col = "#859900")
lines(c(5, 6), c(10, 16), lty="dashed")
lines(guilhem, col = "#B58900")
lines(c(7, 8), c(14, 11), lty="dashed")
lines(alexis, col = "#CB4B16")
lines(c(9, 10), c(12, 11), lty="dashed")
lines(maxime, col = "#DC322F")
lines(c(11, 12), c(8, 11), lty = "dashed")
lines(guillaume, col = "#D33682")
points(benjamin, col = "#2AA198")
points(francois, col = "#859900")
points(guilhem, col = "#B58900")
points(alexis, col = "#CB4B16")
points(maxime, col = "#DC322F")
points(guillaume, col = "#D33682")

dev.off()
