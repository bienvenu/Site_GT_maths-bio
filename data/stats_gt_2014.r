png("stats_gt_2014.png")

par(bg = "#002B36", fg = "#839496", col = "#839496",
    col.axis ="#839496", col.lab ="#839496", col.main ="#839496",
    pch = 19)

plot(c(), c(), xlim = c(1, 13), xlab = "Séance",
     ylim = c(0, 20), ylab = "Nombre de personnes",
     main = "Participation au GT (2014)")

gaetan = data.frame(x = c(1, 2), y = c(17, 8))

farouk = data.frame(x = c(3, 4, 5), y = c(10, 15, 9))

maxime = data.frame(x = c(6, 7), y = c(6, 4))

francois_1 = data.frame(x = c(8), y = c(6))

guillaume = data.frame(x = c(9, 10), y = c(5, 4))

francois_2 = data.frame(x = c(11, 12, 13), y = c(5, 5, 6))

lines(gaetan, col = "#2AA198")
lines(c(2, 3), c(8, 10), lty="dashed")
lines(farouk, col = "#859900")
lines(c(5, 6), c(9, 6), lty="dashed")
lines(maxime, col = "#B58900")
lines(c(7, 8), c(4, 6), lty="dashed")
lines(c(8, 9), c(6, 5), lty="dashed")
lines(guillaume, col = "#DC322F")
lines(c(10, 11), c(4, 5), lty = "dashed")
lines(francois_2, col = "#D33682")
points(gaetan, col = "#2AA198")
points(farouk, col = "#859900")
points(maxime, col = "#B58900")
points(francois_1, col = "#CB4B16")
points(guillaume, col = "#DC322F")
points(francois_2, col = "#D33682")

dev.off()
