png("stats_gt_2015.png")

par(bg = "#002B36", fg = "#839496", col = "#839496",
    col.axis ="#839496", col.lab ="#839496", col.main ="#839496",
    pch = 19)

plot(c(), c(), xlim = c(1, 13), xlab = "Séance",
     ylim = c(0, 20), ylab = "Nombre de personnes",
     main = "Participation au GT (2015)")

camille_1 = data.frame(x = c(1), y = c(17))

felix = data.frame(x = c(2), y = c(12))

camille_2 = data.frame(x = c(3, 4), y = c(14, 14))

francois_1 = data.frame(x = c(5, 6), y = c(11, 13))

marc = data.frame(x = c(7), y = c(10))

guillaume_simon = data.frame(x = c(8, 9), y = c(10, 8))

francois_2 = data.frame(x = c(10), y = c(8))

guilhem = data.frame(x = c(11, 12), y = c(9, 5))

f_prime = data.frame(x = c(13), y = c(6))

lines(c(1, 2, 3), c(17, 12, 14), lty="dashed")
lines(camille_2, col = "#B58900")
lines(c(4, 5), c(14, 11), lty="dashed")
lines(francois_1, col = "#CB4B16")
lines(c(6, 7), c(13, 10), lty="dashed")
lines(c(7, 8), c(10, 10), lty="dashed")
lines(guillaume_simon, col = "#D33682")
lines(c(9, 10), c(8, 8), lty = "dashed")
lines(c(10, 11), c(8, 9), lty = "dashed")
lines(guilhem, col = "#268BD2")
lines(c(12, 13), c(5, 6), lty = "dashed")
points(camille_1, col = "#2AA198")
points(felix, col = "#859900")
points(camille_2, col = "#B58900")
points(francois_1, col = "#CB4B16")
points(marc, col = "#DC322F")
points(guillaume_simon, col = "#D33682")
points(francois_2, col = "#6C71C4")
points(guilhem, col = "#268BD2")
points(f_prime, col = "#2AA198")

dev.off()
