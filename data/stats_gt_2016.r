png("stats_gt_2016.png")

par(bg = "#002B36", fg = "#839496", col = "#839496",
    col.axis ="#839496", col.lab ="#839496", col.main ="#839496",
    pch = 19)

plot(c(), c(), xlim = c(1, 13), xlab = "Séance",
     ylim = c(0, 20), ylab = "Nombre de personnes",
     main = "Participation au GT (2016)")

felix = data.frame(x = c(1, 2, 3), y = c(9, 11, 6))

francois_b = data.frame(x = c(4), y = c(6))

camille = data.frame(x = c(5, 6), y = c(7, 6))

guilhem = data.frame(x = c(7), y = c(5))

benjamin = data.frame(x = c(8), y = c(7))

francois_j = data.frame(x = c(9, 10), y = c(9, 7))

lines(felix, col = "#2AA198")
lines(c(3, 4), c(6, 6), lty="dashed")
lines(c(4, 5), c(6, 7), lty="dashed")
lines(camille, col = "#B58900")
lines(c(6, 7), c(6, 5), lty="dashed")
lines(c(7, 8), c(5, 7), lty="dashed")
lines(c(8, 9), c(7, 9), lty="dashed")
lines(francois_j, col = "#D33682")
#lines(francois_1, col = "#CB4B16")
#lines(c(7, 8), c(10, 10), lty="dashed")
#lines(guillaume_simon, col = "#D33682")
#lines(c(9, 10), c(8, 8), lty = "dashed")
#lines(c(10, 11), c(8, 9), lty = "dashed")
#lines(guilhem, col = "#268BD2")
#lines(c(12, 13), c(5, 6), lty = "dashed")
points(felix, col = "#2AA198")
points(francois_b, col = "#859900")
points(camille, col = "#B58900")
points(guilhem, col = "#CB4B16")
points(benjamin, col = "#DC322F")
points(francois_j, col = "#D33682")
#points(francois_2, col = "#6C71C4")
#points(guilhem, col = "#268BD2")
#points(f_prime, col = "#2AA198")

dev.off()
