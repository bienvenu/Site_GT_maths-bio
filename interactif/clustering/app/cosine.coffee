# SVG options 
width = 500
height = 300
radius = 5
padding = 25



draw_cosine = d3.select(".cosine")
    .append("svg")
    .attr("width", width)
    .attr("height", height);    

# functions
sum = (array) ->
    return array.reduce((previousValue, currentValue) ->
        currentValue + previousValue)
    

distanceP3 = (p,a,b) ->
    r = 0 
    for i in [0..2]
            r += Math.pow(Math.abs(a[i][1]-b[i][1]),p)
    return Math.pow(r,1/p)

round_two = (x) ->
    return Math.round(x*100) / 100

distanceCos = (a,b) ->
    n = 0
    for i in [0..2]
        n += a[i][1] * b[i][1] 

    as = 0
    bs = 0
    for ai in a
        as += ai[1]*ai[1]
    for bi in b
        bs += bi[1]*bi[1]
    d = Math.sqrt(as) * Math.sqrt(bs)

    return 1 - n/d
# Data
#

which_norm = 2

profil1 = [[100,100],[250,200],[400,100]]
profil2 = [[100,200],[250,100],[400,200]]

dist_m = round_two(distanceP3(which_norm, profil1,profil2))
dist_c = round_two(distanceCos(profil1,profil2))
 
# Interactivity functions
move_cosine = (d) ->
    d3.select(this)
        .attr("cy", d[1] = Math.max(Math.min(d[1] += d3.event.dy,height-padding-radius),padding))
        update_cosine()

drag = d3.behavior.drag().origin(Object).on("drag", move_cosine)


# Objects

draw_cosine.append("line")
    .attr("x1", padding+40)
    .attr("y1", padding)
    .attr("x2", padding+40)
    .attr("y2", height-padding)
    .attr("class","axis");

draw_cosine.append("line")
    .attr("x1", width-padding-70)
    .attr("y1", height-padding)
    .attr("x2", padding+40)
    .attr("y2", height-padding)
    .attr("class","axis");

draw_cosine.append("text")
    .attr("x", width-padding-100)
    .attr("y", height)
    .attr("text-anchor", "end")
    .attr("class","tick")
    .text("Conditions")

draw_cosine.append("text")
    .attr("transform","rotate(-90)")
    .attr("x", padding-100)
    .attr("y", padding+10)
    .attr("text-anchor", "end")
    .attr("class","tick")
    .text("Niveau d'expression")


draw_cosine.selectAll(".g1")
    .data(profil1)
    .enter().append("circle")
    .attr("class","ball g1")
    .attr("cx", (d) -> d[0])
    .attr("cy", (d) -> d[1])
    .attr("r", radius)
    .call(drag)

draw_cosine.selectAll(".g2")
    .data(profil2)
    .enter().append("circle")
    .attr("class","ball g2")
    .attr("cx", (d) -> d[0])
    .attr("cy", (d) -> d[1])
    .attr("r", radius)
    .call(drag)

d3.select("#d_deux")
    .text(dist_m)
d3.select("#d_cos")
    .text(dist_c)
 
update_cosine_ball = ()->
    draw_cosine.selectAll(".g1")
        .data(profil1)
        .attr("class","ball g1")
        .attr("cx", (d) -> d[0])
        .attr("cy", (d) -> d[1])
        .attr("r", radius)
    

    draw_cosine.selectAll(".g2")
        .data(profil2)
        .attr("class","ball g2")
        .attr("cx", (d) -> d[0])
        .attr("cy", (d) -> d[1])
        .attr("r", radius)


update_cosine = () -> 
    dist_m = round_two(distanceP3(which_norm, profil1,profil2))
    dist_c = round_two(distanceCos(profil1,profil2))
    d3.select("#d_deux")
        .text(dist_m)
    d3.select("#d_cos")
        .text(dist_c)
 

d3.select("#b_antico")
     .on("click", () ->

        profil1 = [[100,100],[250,200],[400,100]]
        profil2 = [[100,200],[250,100],[400,200]]

        update_cosine_ball()
        update_cosine())

d3.select("#b_trans")
     .on("click", () ->
        
        profil1 = [[100,50],[250,150],[400,250]]
        profil2 = [[100,25],[250,125],[400,225]]

        update_cosine_ball()
        update_cosine())
        
    


    
