data = [[100,100],[200,200]]
colors = ["blue","green","red","yellow"]
dxdy = [0,0]


width = 650
height = 300
radius = 20

    
move = (d) ->
    d3.select(this)
      .attr("cx", d[0] += d3.event.dx ) #Math.max(radius, Math.min(width - radius, d3.event.x)))
      .attr("cy", d[1] += d3.event.dy) #Math.max(radius, Math.min(height - radius, d3.event.y)))
    update()

drag = d3.behavior.drag().origin(Object).on("drag", move)

svgTEST = d3.select(".chart")
    .append("svg")
    .attr("width", width)
    .attr("height", height);    
    
svgTEST.selectAll("circle")
    .data(data)
    .enter().append("circle")
    .attr("class","ball")
    .attr("cx", (d) ->  d[0])
    .attr("cy", (d) ->  d[1])
    .attr("r", radius)
    .call(drag)

svgTEST.append("line")
    .attr("x1", data[0][0])
    .attr("y1", data[0][1])
    .attr("x2", data[1][0])
    .attr("y2", data[1][1])
    .attr("id", "d2")
    .attr("class","d2");

svgTEST.append("line")
    .attr("x1", data[0][0])
    .attr("y1", data[1][1])
    .attr("x2", data[1][0])
    .attr("y2", data[1][1])
    .attr("id", "d1_1")
    .attr("class","d1");

svgTEST.append("line")
    .attr("x1", data[0][0])
    .attr("y1", data[0][1])
    .attr("x2", data[0][0])
    .attr("y2", data[1][1])
    .attr("id", "d1_2")
    .attr("class","d1");


norme = (p,dx,dy) ->
    return Math.round(Math.pow(Math.pow(dx,p)+ Math.pow(dy,p),1/p ) *100 )/ 100

update = () ->
    dx = Math.abs(data[0][0]-data[1][0])
    dy = Math.abs(data[0][1]-data[1][1])

    d3.select("#d2")
        .attr("x1", data[0][0])
        .attr("y1", data[0][1])
        .attr("x2", data[1][0])
        .attr("y2", data[1][1])

    d3.select("#d1_1")
        .attr("x1", data[0][0])
        .attr("y1", data[1][1])
        .attr("x2", data[1][0])
        .attr("y2", data[1][1])

    d3.select("#d1_2")
        .attr("x1", data[0][0])
        .attr("y1", data[0][1])
        .attr("x2", data[0][0])
        .attr("y2", data[1][1])


    d3.select("#dx")
        .text(dx)
    d3.select("#dy")
        .text(dy)
    d3.select("#posA")
        .text(data[0][0]+","+data[0][1])
    d3.select("#posB")
        .text(data[1][0]+","+data[1][1])

    d3.select("#norme1")
        .text(norme(1,dx,dy))
    d3.select("#norme2")
        .text(norme(2,dx,dy))
    d3.select("#norme3")
        .text(norme(3,dx,dy))
    d3.select("#norme10")
        .text(norme(10,dx,dy))
    d3.select("#normeInf")
        .text(Math.max(dx,dy))


update()

