# SVG options 
width = 500
height = 300
offset_left = 40

draw = d3.select(".normal")
    .append("svg")
    .attr("width", width)
    .attr("height", height);    

# functions
sum = (array) ->
    return array.reduce((previousValue, currentValue) ->
        currentValue + previousValue)
    

# Data
data = [([Math.random()*(300-offset_left)+offset_left,270]) for i in [0...8]][0]
max = Math.min.apply(Math,d[0] for d in data)
min = Math.max.apply(Math,d[0] for d in data)


moy = 0
for i in data
    moy += i[0]
moy = moy/data.length

sigma = 0
for i in data
    sigma += Math.pow((i[0] - moy),2)
sigma = Math.sqrt(sigma/data.length)

 
radius = 5
 
# Interactivity functions
move = (d) ->
    d3.select(this)
        .attr("cx", d[0] = Math.max(Math.min(d[0] += d3.event.dx,300-radius),offset_left))
        update()

drag = d3.behavior.drag().origin(Object).on("drag", move)

# Data visualisation functions

scaleStandard = (x) ->
    return 240-(((x-moy)/(sigma))+2)*(210/5)+30/5

scale = (x) ->
    return ((x-min)/(max-min))*210+30


# Objects

draw.append("line")
    .attr("x1", offset_left)
    .attr("y1", 250)
    .attr("x2", 300)
    .attr("y2", 250)
    .attr("class","axis");

draw.append("line")
    .attr("x1", offset_left)
    .attr("y1", 20)
    .attr("x2", offset_left)
    .attr("y2", 250)
    .attr("class","axis");

draw.append("line")
    .attr("x1", min)
    .attr("y1", 30)
    .attr("x2", max)
    .attr("y2", 240)  
    .attr("id","normalize")
    .attr("class","normalize");
                         
draw.append("text")
    .attr("x", offset_left)
    .attr("y", scale(max))
    .attr("text-anchor", "end")
    .attr("class","tick normalize")
    .text("0")

draw.append("text")
    .attr("x", offset_left)
    .attr("y", scale(min))
    .attr("text-anchor", "end")
    .attr("class","tick normalize")
    .text("1")

draw.append("text")
    .attr("x", offset_left)
    .attr("y", 15)
    .attr("text-anchor", "begin")
    .attr("class","tick standardize")
    .text("Z-score")


draw.append("text")
    .attr("x", offset_left)
    .attr("y", scaleStandard(moy))
    .attr("text-anchor", "end")
    .attr("class","tick standardize")
    .text("0")

draw.append("text")
    .attr("x", offset_left)
    .attr("y", scaleStandard(max))
    .attr("text-anchor", "end")
    .attr("class","tick standardize")
    .attr("id","standardMaxTick")
    .text(Math.round((max-moy)/(sigma)*10)/10)

draw.append("text")
    .attr("x", offset_left)
    .attr("y", scaleStandard(min))
    .attr("text-anchor", "end")
    .attr("class","tick standardize")
    .attr("id","standardMinTick")
    .text(Math.round((min-moy)/(sigma)*10)/10)




draw.selectAll("circle")
    .data(data)
    .enter().append("circle")
    .attr("class","ball")
    .attr("cx", (d) -> d[0])
    .attr("cy", (d) -> d[1])
    .attr("r", radius)
    .call(drag)



draw.selectAll(".lv")
    .data(data)
    .enter().append("line")
    .attr("class","lv normalize")
    .attr("x1", (d) -> d[0])
    .attr("y1", (d) -> 250)
    .attr("x2", (d) -> d[0])
    .attr("y2", (d) -> scale(d[0]))

draw.selectAll(".lh")
    .data(data)
    .enter().append("line")
    .attr("class","lh normalize")
    .attr("x1", (d) -> offset_left)
    .attr("y1", (d) -> scale(d[0]))
    .attr("x2", (d) -> d[0])
    .attr("y2", (d) -> scale(d[0]))
   


draw.selectAll(".sv")
    .data(data)
    .enter().append("line")
    .attr("class","sv standardize")
    .attr("x1", (d) -> d[0])
    .attr("y1", (d) -> 250)
    .attr("x2", (d) -> d[0])
    .attr("y2", (d) -> scaleStandard(d[0]))

draw.selectAll(".sh")
    .data(data)
    .enter().append("line")
    .attr("class","sh standardize")
    .attr("x1", (d) -> offset_left)
    .attr("y1", (d) -> scaleStandard(d[0]))
    .attr("x2", (d) -> d[0])
    .attr("y2", (d) -> scaleStandard(d[0]))

draw.append("line")
    .attr("x1", min)
    .attr("y1", scaleStandard(min))
    .attr("x2", max)
    .attr("y2", scaleStandard(max))  
    .attr("id","regStandard")
    .attr("class","standardize");
    
 


update = () -> 
    max = Math.min.apply(Math,d[0] for d in data)
    min = Math.max.apply(Math,d[0] for d in data)

    moy = 0
    for i in data
        moy += i[0]
    moy = moy/data.length

    sigma = 0
    for i in data
        sigma += Math.pow((i[0] - moy),2)
    sigma = Math.sqrt(sigma/data.length)
     


    draw.select("#standardMinTick")
        .attr("y", scaleStandard(min))
        .text(Math.round((min-moy)/(sigma)*10)/10)
    draw.select("#standardMaxTick")
        .attr("y", scaleStandard(max))
        .text(Math.round((max-moy)/(sigma)*10)/10)
        
    d3.select("#normalize")
        .attr("x1", min)
        .attr("y1", 30)
        .attr("x2", max)
        .attr("y2", 240)

    d3.select("#regStandard")
        .attr("x1", min)
        .attr("y1", scaleStandard(min))
        .attr("x2", max)
        .attr("y2", scaleStandard(max))  

  
    draw.selectAll(".lv")
        .attr("x1", (d) -> d[0])
        .attr("x2", (d) -> d[0])
        .attr("y2", (d) -> scale(d[0]))

    draw.selectAll(".lh")
        .attr("y1", (d) -> scale(d[0]))
        .attr("x2", (d) -> d[0])
        .attr("y2", (d) -> scale(d[0]))

    draw.selectAll(".sv")
        .attr("x1", (d) -> d[0])
        .attr("x2", (d) -> d[0])
        .attr("y2", (d) -> scaleStandard(d[0]))

    draw.selectAll(".sh")
        .attr("y1", (d) -> scaleStandard(d[0]))
        .attr("x2", (d) -> d[0])
        .attr("y2", (d) -> scaleStandard(d[0]))

display_normalize = 1
d3.select("#bnorm")
     .on("click", () ->
        if display_normalize == 1
            display_normalize = 0
            draw.selectAll(".normalize")
                .transition().style("visibility", "hidden")
        else
            display_normalize = 1
            draw.selectAll(".normalize")
                .transition().style("visibility", "visible"))
        
    
display_standardize = 1
d3.select("#bstand")
     .on("click", () ->
        if display_standardize == 1
            display_standardize = 0
            draw.selectAll(".standardize")
                .transition().style("visibility", "hidden")
        else
            display_standardize = 1
            draw.selectAll(".standardize")
                .transition().style("visibility", "visible"))
        
    


    
