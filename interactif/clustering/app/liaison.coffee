# SVG options 
width = 500
height = 300
padding = 10
radius = 5

draw_cl = d3.select(".critere_liaison")
    .append("svg")
    .attr("width", width)
    .attr("height", height);    

# functions
sum = (array) ->
    return array.reduce((previousValue, currentValue) ->
        currentValue + previousValue)
    

# Data
G1 = [([Math.random()*(width/2-padding)+padding+width/2,Math.random()*(height-padding)+padding]) for i in [0...3]][0]
G2 = [([Math.random()*(width-padding)+padding,Math.random()*(height-padding)+padding]) for i in [0...4]][0]

distanceP = (p,a,b) ->
    dx = Math.abs(a[0]-b[0])
    dy = Math.abs(a[1]-b[1])
    return Math.pow(Math.pow(dx,p)+ Math.pow(dy,p),1/p)

round_two = (x) ->
    return Math.round(x*100) / 100

p_norm = 2
dist = []
for e1 in G1
    for e2 in G2
      dist.push(distanceP(p_norm,e1,e2))
single_l = round_two(Math.min.apply(Math,d for d in dist))
complete_l = round_two(Math.max.apply(Math,d for d in dist))
upgma = 0
for i in dist
    upgma += i
upgma = round_two(upgma/dist.length)

d3.select("#v_upgma")
    .text(upgma)
d3.select("#v_single_l")
    .text(single_l)
d3.select("#v_complete_l")
    .text(complete_l)

       
    
# Interactivity functions
move_liaison = (d) ->
    d3.select(this)
        .attr("cx", d[0] = Math.max(Math.min(d[0] += d3.event.dx,width-padding-radius),padding))
        .attr("cy", d[1] = Math.max(Math.min(d[1] += d3.event.dy,height-padding-radius),padding))
        update_liaison()

drag = d3.behavior.drag().origin(Object).on("drag", move_liaison)

# Data visualisation functions

# Objects

draw_cl.selectAll(".g1")
    .data(G1)
    .enter().append("circle")
    .attr("class","g1 ball")
    .attr("cx", (d) -> d[0])
    .attr("cy", (d) -> d[1])
    .attr("r", radius)
    .call(drag)

draw_cl.selectAll(".g2")
    .data(G2)
    .enter().append("circle")
    .attr("class","g2 ball")
    .attr("cx", (d) -> d[0])
    .attr("cy", (d) -> d[1])
    .attr("r", radius)
    .call(drag)



update_liaison = () ->
    dist = []
    for e1 in G1
        for e2 in G2
          dist.push(distanceP(p_norm,e1,e2))
    single_l = round_two(Math.min.apply(Math,d for d in dist))
    complete_l = round_two(Math.max.apply(Math,d for d in dist))
    upgma = 0
    for i in dist
        upgma += i
    upgma = round_two(upgma/dist.length)

    d3.select("#v_upgma")
        .text(upgma)
    d3.select("#v_single_l")
        .text(single_l)
    d3.select("#v_complete_l")
        .text(complete_l)

    return 0


    
d3.select("#v_norme")
     .on("change", (v) ->
        p_norm = this.options[this.selectedIndex].value
        update_liaison())
   
