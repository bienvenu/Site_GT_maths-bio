\section{Introduction}

Tout le monde a déjà dans sa vie rencontré une équation. Il s'agit tout
simplement d'une égalité entre deux membres, qui nous donne des indications sur
un objet (l'inconnue), qui nous décrit son comportement. Résoudre une équation
n'est rien d'autre que trouver tous les objets qui se comportent de cette
façon. Des exemples simples d'équations peuvent être :
\[
  2x^2 - 8 = 0
\]
Ou encore :
\[
  f'' +  f' = \cos
\]
On voit tout de suite que bien qu'il s'agisse de deux équations, elles n'ont
pas la même nature. On peut classer les équations en différentes catégories.
Par exemple, la première équation est ce que l'on appelle une équation
algébrique, son inconnue est un élément de $\mathbb{C}$, alors que la seconde
est une équation différentielle, où l'inconnue est une fonction.  \\
\bigformatpagebreak

On ne dispose pas des mêmes outils pour résoudre ces différents types
d'équations. En général, les équations algébriques sont plus simples à résoudre
que les équations différentielles, même si ce n'est pas toujours le cas. La
transformée de Laplace est un outil qui peut permettre de résoudre plus
facilement certaines équations différentielles en les ``transformant" en
équations algébriques. Cette caractéristique l'a rendue très utilisée dans
plusieurs branches de la physique, pour l'étude de systèmes dynamiques.  \\ 

La transformée de Laplace ne se résume bien entendu pas qu'à son rôle dans la
résolution d'équations. C'est un objet mathématique à part entière qui a été
très étudié, et intervient dans bien d'autres domaines. Historiquement, on
retrouve l'utilisation d'une forme similaire à la transformée de Laplace
actuelle dans des travaux du célèbre Leonhard Euler, au XVIII\textsuperscript{e}
siècle. C'est cependant le scientifique Laplace qui donnera son nom à cette
transformée, en utilisant une forme discrète appelée transformée en Z lors de
ses travaux sur les probabilités. Son intérêt dans la résolution des équations
différentielles n'arrive que bien plus tard, à la fin du XIX\textsuperscript{e}
siècle, et serait dû à Oliver Heaviside, un ingénieur anglais. Il ne
l'utilisait cependant que comme une ``règle de calcul" pour simplifier les
équations. La formalisation de l'outil a été réalisée encore plus tard, au
début du XX\textsuperscript{e} siècle.  \\

Nous allons ici essayer de redéfinir ce qu'est la transformée de Laplace, et
donner ses propriétés les plus importantes. Nous allons l'aborder à l'aide
d'une de ses caractéristiques, qui est de transformer un produit de convolution
en produit. Mais alors une première question se pose : qu'est-ce que la
convolution ?


\section{La convolution}

\subsection{Intuition sur les systèmes linéaires}

Dans la théorie du signal notamment, on peut appeler système une ``entité" qui
prend une ou plusieurs entrées, et qui ressort une ou plusieurs sorties. Par
exemple, en biologie, on peut voir un neurone comme un système. On lui donne en
entrée tous les flux d'ions provoqués par ses synapses, et le neurone ressort
un pattern de potentiels d'action. Ou un autre exemple pourrait être les
cellules $\beta$ pancréatiques : on leur donne en entrée la glycémie au niveau
du pancréas, et la cellule donne en sortie une quantité de molécules d'insuline
sécrétées (une seule entrée et une seule sortie cette fois). Ce que l'on
appelle système est en fait la modélisation d'un procédé, c'est une
représentation théorique qu'on s'en fait pour pouvoir l'étudier. \\

Nous allons pour l'instant nous intéresser aux systèmes simples à une entrée et
une sortie. On peut les représenter de cette façon :

\begin{center}
  \includegraphics[width=0.4\linewidth]{fig2014/GJeanne2014s1-2_fig01.pdf}
\end{center}

Si le système possède certaines caractéristiques, on va pouvoir le classer dans
des catégories. Les systèmes qui vont nous intéresser sont les systèmes dits
linéaires. Pour qu'un système soit linéaire, il faut que si une entrée $u$
donne une sortie $y$, et qu'une entrée $v$ donne une sortie $w$, alors une
entrée $u+v$ donne une sortie $y+w$. De plus, il faut qu'une entrée $\lambda u$
donne une sortie $\lambda y$. Par exemple, prenons le système qui, à la fin
de l'année, calcule vos intérêts sur un compte bancaire à taux fixe : Si l'année
d'après vous avez deux fois plus d'argent sur votre compte, vous aurez deux
fois plus d'intérêts, et si vous mettez votre argent en commun avec un ami,
vous gagnerez autant à deux que si vous aviez deux comptes séparés. Des
exemples tirés de la biologie ne sont pas simples à trouver, car les systèmes
linéaires sont assez simples, à l'opposé des systèmes biologiques plutôt
complexes. Modéliser un procédé biologique par un système linéaire est souvent
très réducteur et peu intuitif.\\

Ce qui nous intéresse pour accéder à la convolution va nécessiter une légère
introduction à la théorie du signal. On appelle signal l'évolution dans le
temps d'une grandeur. Un système va alors permettre de transformer ce signal en
un autre signal. Par exemple, si l'on se place en temps discret et que l'on
étudie la désintégration d'un atome radioactif, de constante de temps $\tau$.
On peut considérer un système qui nous donne l'évolution au cours du temps
d'une quantité initiale d'atomes. Dans ce cas, le signal d'entrée est, à chaque
pas de temps, la quantité d'atomes radioactifs introduite dans le système, et
la sortie est combien il reste d'atomes à chaque pas de temps. Imaginons que
l'on mette une mole d'atomes au temps $t=0$ et qu'on la laisse évoluer. L'entrée
est alors :
\[
  u : 
  \left\{ 
  \begin{array}{l}
    \mathbb{Z} \rightarrow \mathbb{R}\\
          0 \mapsto 1 \\
          k \mapsto 0 \text{ si } k \ne 0\\
  \end{array}
  \right.
\]
Et la sortie est simplement :
\[
  y :
  \left\{ 
  \begin{array}{l}
    \mathbb{Z} \rightarrow \mathbb{R}\\
          k \mapsto e^{-k/\tau} \text{ si } k \geq 0\\
          k \mapsto 0 \text{ sinon}\\
  \end{array}
  \right.
\]
Si on met deux fois plus d'atomes au temps $t=0$, la sortie sera tout
simplement deux fois plus grande. De même, si on rajoute 2 moles dans le
système à un autre temps, la sortie sera la somme des deux réponses
élémentaires. Ce système est en fait linéaire. Rajouter 2 moles dans le système
à $t=2$ revient à prendre comme fonction d'entrée :
\[
  v : 
  \left\{ 
  \begin{array}{l}
    \mathbb{Z} \rightarrow \mathbb{R}\\
          0 \mapsto 1\\
          2 \mapsto 2\\
          k \mapsto 0 \text{ si } k \ne 0 \text{, } 2\\
  \end{array}
  \right.
\]
On peut facilement exprimer $v$ en fonction de $u$ :
\[
  \forall k \in \mathbb{Z} \text{, } v(k) = u(k) + 2u(k-2) \, , 
\]
et comme le système est linéaire, on peut calculer la réponse $w$ à $v$ en
fonction de $y$ :
\[
  \forall k \in \mathbb{Z} \text{, } w(k) = y(k) + 2y(k-2) \, .
\]
Ici, $y$ est en fait ce que l'on pourrait appeler la ``réponse élémentaire" du
système, c'est la façon dont le système se comporte si on lui donne une entrée
unitaire uniquement en $0$. On voit bien que par le même raisonnement qu'au
dessus, quelque que soit l'entrée, on va pouvoir écrire sa sortie uniquement en
fonction de $y$. En fait, on peut même écrire :
\[
  \forall k \in \mathbb{Z} \text{, } w(k) =
  \sum_{t=-\infty}^{+\infty}{v(t)y(k-t)} \, , 
\]
où $v$ est une entrée quelconque et $w$ sa sortie. C'est cette opération que
l'on nomme la convolution (dans le cas discret ici).\\

En faisant tendre le pas de temps vers 0, on obtient la formule de la
convolution en temps continu :
\[
  w(k) = \int_{-\infty}^{+\infty}{v(t)\, y(k-t)\, dt}
\]
On note la convolution $*$. Ainsi, ici, $w = v * y$.\\\bigformatenlarge{5.5ex}

\textbf{Remarque :} lorsque l'on passe du discret au continu, l'entrée $v$ et
la réponse $y$ ne sont plus les mêmes fonctions. Pour reprendre notre exemple
en continu, \smallformatpagebreak le fait d'ajouter à un temps $t$ une quantité
d'atomes radioactifs revient à placer un Dirac\footnote{Informellement, on peut
voir un Dirac en t comme une ``fonction" qui vaut 0 partout, sauf en $t$ où
elle prend une valeur infinie de manière à ce que son intégrale sur
$\mathbb{R}$ vaille 1 -- nous y reviendrons.} centré sur le temps $t$. Et la
réponse $y$ est en fait désormais la réponse à cette impulsion de Dirac, et
serait une exponentielle décroissante partant de $0$. \\

Nous avons maintenant vu ce qu'était la convolution. Dans un système linéaire,
on est capables d'obtenir facilement les réponses à n'importe quelle entrée si
on connaît la réponse du système à un signal ``élémentaire" (la décroissance
exponentielle dans notre exemple). L'opération qui permet de la trouver est
alors la convolution. Voyons donc à présent un exemple plus précis de produit
de convolution avant d'en étudier les caractéristiques. \smallformatpagebreak 

%Nous allons maintenant voir quelques propriétés de ce produti de convolution
%avant de l'utiliser pour arriver jusqu'aux transformées de Laplace. 

\subsection{Un exemple : le porte à porte}

Un exemple de convolution assez classique est celui de la convolution par une
porte. Une ``porte" n'est rien d'autre qu'une fonction nulle sur $\mathbb R$
sauf sur un intervalle où elle vaut 1. Par exemple, les fonctions $u$ et $h$
représentées ci-dessous sont deux portes.

\begin{center}
  \includegraphics[width=0.9\linewidth]{fig2014/GJeanne2014s1-2_fig02.pdf}
\end{center}

Intéressons nous à présent à la convolution de ces deux portes, $y = u * h$.\\

Par définition, on a :
\[
  y(t) = \int_{-\infty}^{+\infty}{u(\tau)h(t-\tau)d\tau} 
\]
La représentation de $\tau \mapsto h(t-\tau )$ est (attention ici $t$ est un
paramètre, et la variable est $\tau$): 

\begin{center}
  \includegraphics[width=0.45\linewidth]{fig2014/GJeanne2014s1-2_fig03.pdf}
\end{center}

La fonction sous l'intégrale vaut 1 à l'intersection des deux
supports\footnote{Le support d'une fonction désigne l'ensemble des éléléments
de l'espace de départ pour lesquels elle n'est pas nulle.}, et 0 partout
ailleurs. La fonction $y$ vaut alors la longueur de cette intersection. Ainsi
quand $t$ est trop petit, $y$ vaut 0 puisque les deux supports sont disjoints.
Puis,  quand $t$ augmente, l'intersection devient non nulle, et sa longueur
augmente linéairement jusqu'à $t = -0.5$, moment à partir duquel le support de
$\tau \mapsto h(t - \tau)$ est inclus dans celui de $u$. La longueur de
l'intersection des deux supports reste alors constante égale à 1 jusqu'à ce que
$t = 0.5$, ou elle rediminue linéairement jusqu'à ce que $t = 1.5$.  Ainsi la
représentation de $y$ est :

\begin{center}
  \includegraphics[width=0.6\linewidth]{fig2014/GJeanne2014s1-2_fig04.pdf}
\end{center}

Si on considère que $y$ correspond à un niveau de blanc d'une image en 1D, si
on fait passer une image initiale $u$ par une porte $h$, on obtient une image
$y$ qui est floutée. Faire passer une image par une porte revient en fait à la
flouter (ici le flou est ``linéaire").

\begin{center}
  \includegraphics[width=\linewidth]{fig2014/GJeanne2014s1-2_fig05.png}
\end{center}

\textbf{Remarque :} Il existe d'autres méthodes pour flouter les images. Par
exemple, on peut convoluer par autre chose que des portes : des bosses, etc.\\

À présent que nous avons vu un premier exemple de calcul de produit de
convolution, il est temps de voir quelques caractéristiques de celui-ci.

\subsection{Propriétés du produit de convolution}

\begin{enumerate}[(i)]
  \item Commutativité : $u \ast h = h \ast u$
  \item Associativité : $(u \ast h) \ast g = u \ast (h \ast g)$
  \item Distributivité : $u \ast (h+g) = u \ast h + u \ast g$
\end{enumerate}

Il est assez naturel de se demander quand on définit une opération si celle-ci
possède un élément neutre, c'est-à-dire si $\exists e \text{ tq } h \ast e = e
\ast h = h$.  Pour commencer à entrevoir la réponse, prenons une fonction
particulière, par exemple pour un $a$ dans $\mathbb{R}$ considérons :
\[
  f_a :\,  t \mapsto
  \left\{ 
  \begin{array}{l}
          1 \text{ si } t \in [-a,a]\\
          0 \text{ sinon}\\
  \end{array}
  \right.
\]
On doit alors avoir :
\[ \smallermaths  
  \forall t \in \mathbb{R} \text{, } 
  \big(f_a * e\big)(t) = 
  \int_{-\infty}^{+\infty}{f_a(\tau)\, e(t-\tau)\, d\tau} = 
  \int_{-a}^{a}{e(t-\tau)\, d\tau} = f_a(t)
\]
En particulier, pour $t = 0$, comme $\forall a \in \mathbb{R}$, $f_a(0) = 1$ on
a : 
\[
  \int_{-a}^a{e(-\tau)\, d\tau} = 1
\]
En effectuant le changement de variable $x = - \tau$ on a :
\[
  \int_{-a}^{a}{e(x)\, dx} = 1
\]
Ceci est vrai pour tout $a \in \mathbb R$. On voit bien qu'en faisant varier
ce paramètre $a$, l'aire doit rester constante, donc sur tout segment ne
contenant pas 0, l'aire doit être nulle, et ainsi la fonction elle aussi doit
être nulle. Et pourtant, lorsque le segment comporte 0, l'aire doit valoir 1. \\

Notre élément neutre ressemblerait donc à une fonction nulle partout sauf en 0,
où elle vaudrait l'infini, mais de telle manière que l'aire sous la courbe ``en
0" reste de 1.  Cet objet n'est en fait pas une fonction, car une fonction
(réelle) ne peut pas prendre de valeur infinie en un point. Il s'agit d'un
autre type d'objet mathématique, appelé \emph{distribution}, qui généralise les
fonctions.\\

Par ce raisonnement, on voit bien qu'il n'y a pas de fonction qui soit
l'élément neutre de la convolution. On pourrait redéfinir la convolution sur
les distributions, il existerait alors bien un élément neutre, qui est la
distribution de Dirac, notée $\delta$. Un objet qui serait nul partout sauf en
0, et dont l'intégrale sur $\mathbb{R}$ vaudrait 1 serait élément neutre de la
convolution, mais un tel élément n'existe pas parmi les fonctions.\\

Toutes les propriétés du produit de convolution qui viennent d'être présentées
rappellent fortement celles du produit ``classique". Il serait intéressant de
trouver une manière de passer du produit de convolution au produit classique,
et c'est là qu'intervient la transformation de Laplace...\\

Le point de vue adopté jusqu'ici est celui du traitement du signal, où on ne
s'intéresse qu'à des fonctions nulles pour les temps négatifs. Ainsi par la
suite on ne prendra plus que les intégrales sur $\mathbb{R}_+$ pour cette
raison.


\section{Laplace est dans la place}

Nous allons ici essayer de retrouver la transformée de Laplace à l'aide d'une
de ses caractéristiques qui est de transformer les produits de convolution en
produits classiques. 

\subsection{Rappel : comment passer d'un `$\times$' à un `$+$'}

Bien que ce ne soit pas au coeur du débat, nous allons ré-expliquer rapidement
pourquoi les fonctions permettant de passer d'une multiplication à une addition
sont les logarithmes. Nous ne ferons pas la démonstration complète, et
admettrons certaines petites propriétés. Cela constituera une bonne
introduction pour ce qui vient.\\

Nous cherchons une fonction $l$ telle que :
\begin{equation} \label{eqlog}
  l(a.b) = l(a) + l(b)
\end{equation}
Restreignons le domaine sur lequel on va étudier une telle fonction. Il est
inintéressant de définir une telle fonction en 0 car, si $l(0)$ existe :
\[
  \forall a \in \mathbb{R},\: l(0) = l(0\times a) = l(0) + l(a)
\]
Donc,
\[
  \forall a \in \mathbb{R},\: l(a) = 0
\]
De plus, si on définit $l$ sur $\mathbb{R}^*$,
\[
  \forall a \in \mathbb{R},\: l(-a\times -a) = 2l(-a) = l(a\times a) = 2l(a)
\]
Cette fonction serait donc paire sur $\mathbb{R}^*$. On se place donc pour la
suite sur $\mathbb{R}_+^*$ Montrons maintenant qu'une telle fonction est un
logarithme. Nous allons supposer que $l$ est dérivable sur $\mathbb{R}_+^*$. En
dérivant l'équation~\eqref{eqlog} par rapport à $b$, on a :
\[
  \forall a \text{, } a.l'(ab) = l'(b)
\]
En particulier pour $b = 1$ on a :
\[
  \forall a,\,   a.l'(a) = l'(1)\, , 
  \quad\text{càd} \quad
  \forall a,\,  l'(a) = \frac{l'(1)}{a}
\]
C'est la définition d'un logarithme. \\

Nous supposons maintenant que $l$ admet une réciproque, que l'on note $e$.
On a alors :
\[
  e\big(l(ab)\big) = e\big(l(a) + l(b)\big)
\]
c'est-à-dire
\[
  ab = e\big(l(a) + l(b)\big) \, , 
\]
ou encore
\[
  e(l\big(a)\big)\, e\big(l(b)\big) = e\big(l(a) + l(b)\big) \, .
\]
En notant $l(a) = x$ et $l(b) = y$, on peut réécrire l'égalité précédente :
\[
  e(x)\, e(y) = e(x+y) \, .
\]
La réciproque d'un logarithme transforme donc une somme en produit. Pour la
suite de la démonstration nous supposerons que $e$ est continue.\\

Montrons maitenant que les $e$ sont des fonctions exponentielles, càd que:
\[
  \forall x \in \mathbb{R} \text{, } e(x) = e(1)^x
\]
Montrons au préalable que $e$ est strictement positive : supposons que $\exists
x_0$ tel que $e(x_0) = 0$. Alors pour $x \in \mathbb{R}$ :
\[
  e(x) = e(x+x_0-x_0) = e(x-x_0).e(x_0) = 0
\]
Donc $e$ est identiquement nulle. Or on a défini $e$ comme une réciproque, elle
ne peut pas être identiquement nulle. Donc :
\[
  \forall x \in \mathbb{R},\: e(x) \ne 0
\]

Supposons que $\exists x \in \mathbb{R}$ tel que $e(x) < 0$. Par continuité,
comme $e$ n'a pas d'image nulle, elle est négative sur $\mathbb{R}$ entier. Or :
\[
  e(0) = e(-x+x) = e(-x)\, e(x) > 0
\]
Nous arrivons à une contradiction. Ainsi $e$ est strictement positive.\\

Montrons désormais la propriété recherchée sur $\mathbb{N}$, puis sur
$\mathbb{Z}$, puis sur $\mathbb{Q}$ et enfin sur $\mathbb{R}$. \\

\noindent\textbullet \: Sur $\mathbb{N}$ :\\
Soit $n \in \mathbb{N}$, on a :
\[
  e(n).e(1) = e(n+1)
\]
$\big(e(n)\big)_n$ est donc une suite géométrique de raison $e(1)$ et donc :
\[
  \forall n \in \mathbb{N} \text{, } e(n) = e(1)^n 
\]

\noindent\textbullet \: Sur $\mathbb{Z}$ : \\
Montrons que $e(0) = 1$ :
\[
  e(0 + 0) = e(0) = e(0)^2\
\]
On a déjà écarté $e(0) = 0$ donc : 
\[
  e(0)=1
\]
De plus :
\[
  \forall n \in \mathbb{N} \text{, } e(n).e(-n) = e(n-n) = e(0) = 1 
\]
Et comme $e(n) \ne 0$ on a : 
\[
  \forall n \in \mathbb{N} \text{, } e(-n) =
  \frac{1}{e(n)} = \frac{1}{e(1)^n} = e(1)^{-n}
\]
Ainsi :
\[
  \forall k \in \mathbb{Z} \text{, } e(k) = e(1)^k 
\]

\noindent\textbullet \: Sur $\mathbb{Q}$ :\\
Montrons la propriété sur les inverses. Soit $p \in \mathbb{N}^*$ :
$$
        e(1) = e\!\underbrace{\left( \frac{1}{p} + \frac{1}{p} + \cdots +
        \frac{1}{p}\right)}_{p\: \text{fois}} = e\left( \frac{1}{p}\right) ^p
$$
Donc :
\[
  e\left( \frac{1}{p}\right) = e(1)^{\frac{1}{p}}
\]
Maintenant soit $r \in \mathbb{Q}$ i.e.\ $r=\frac{q}{p}$ avec $q \in
\mathbb{Z}$ et $p \in \mathbb{N}^*$. On a alors :
\[
  e(r) = e\left( \sum_{k = 1}^{q}{\frac{1}{p}}\right) =
  e\left(\frac{1}{p}\right)^q = \big(e(1)^{\frac{1}{p}}\big)^q =
  e(1)^\frac{q}{p} = e(1)^r
\]
Donc :
\[
  \forall r \in \mathbb{Q} \text{, } e(r) = e(1)^r
\]
\\

\noindent\textbullet \: Sur $\mathbb{R}$ : \\
Soit $x \in \mathbb{R}$. Comme $\mathbb{Q}$ est dense\footnote{cf notes de
topologie.} dans $\mathbb{R}$,  on dispose d'une suite $(r_n) \in
\mathbb{Q}^{\mathbb{N}}$ telle que $r_n \rightarrow x$.  Comme $e$ est continue, 
on a :
\[
  \lim_{n \rightarrow +\infty}{e(r_n)} =
  e\left( \lim_{n \rightarrow +\infty}{r_n}\right) = e(x)
\]
Or, 
\[
  \lim_{n \rightarrow +\infty}{e(r_n)} = \lim_{n \rightarrow +\infty}{e(1)^{r_n}}
\]
Et par continuité de la fonction $x \rightarrow e(1)^x$ on a :
\[
  \lim_{n \rightarrow +\infty}{e(1)^{r_n}} = e(1)^{ \lim_n{r_n}} = e(1)^x
\]
Finalement,
\[
  \forall x \in \mathbb{R} \text{, } e(x) = e(1)^x
\]
La réciproque d'une fonction logarithme est bien une fonction exponentielle.

\subsection{Lier produit de convolution et produit}

On cherche maintenant une fonction $L$ telle que :
\[
  L(u*h) = L(u).L(h),  \quad\text{càd}\quad
  L\left(\int_{0}^{+\infty}{u(\tau)h(t-\tau)d\tau}\right) = L(u).L(h)
\]
dès que cette intégrale a un sens.\\

On définit $L(u)$, la transformée de Laplace de $u$, de variable
complexe\footnote{En toute rigeur, la transformée de Laplace d'une fonction est
une fonction de $\mathbb{C}$ dans $\mathbb{C}$. Pour $f: \mathbb{C} \to
\mathbb{C}$ et $A\subset \mathbb{R}$, on définit sans difficulté $\int_A f(x)
dx = \int_A \mathrm{Re}\big(f(x)\big)dx + {\boldsymbol i} \int_A
\mathrm{Im}\big(f(x)\big)dx$. Le lecteur ne souhaitant pas se soucier de cela
pourra se limiter au cas $p \in \mathbb{R}$, qui est déjà très instructif.}
$p$, comme :
\[
  L(u)(p) = \int_{0}^{+\infty}{u(t)\, e^{-pt}\, dt}
\]
Insistons sur le fait que la transformée de Laplace d'une fonction est elle-même
une fonction.\\

Pourquoi prendre une telle expression ? On peut essayer d'avoir l'intuition de
cette expression avec le raisonnement suivant : Tout d'abord, prendre
l'intégrale de la convolution permet de transformer le produit de convolution
en produit d'intégrales. De manière très informelle,
\[
  \int{\left( \int{u(\tau )\, u(t-\tau )\,d\tau }\right) dt} =
  \int{u(t)\,dt} \int{u(\tau )\,d\tau } 
\]
Cependant, dans ce cas, il faut que la fonction $u$ soit intégrable. Il semble
assez naturel d'essayer de ``calmer" la fonction avec une fonction décroissante.
Pourquoi alors choisir l'exponentielle ? Tout d'abord multiplier par une
exponentielle décroissante permet d'intégrer de nombreuses fonctions. De plus,
comme nous l'avons vu précédemment, l'exponentielle permet de passer de la
somme au produit, ce qui permet toujours de réaliser la petite manipulation
précédente, puisque
\[
  e^{-t}\,u(\tau )\,u(t-\tau ) =
  e^{-(t-\tau )}\,u(t-\tau ) \times e^{-\tau }\,u(\tau )
\]
En prenant l'intégrale par rapport à $t$ de la convolution, on pourra de
nouveau séparer les intégrales et retrouver un produit. On peut ajouter un
paramètre à l'exponentielle utilisée, et on rerouve l'expression donnée de la
tranformée de Laplace.\\

Vérifions maintenant que la transformée de Laplace transforme bien la
convolution en produit :

\begin{align*}
L(u*h) &= L\left(\int_{0}^{+\infty}{u(\tau)\,h(t-\tau)\, d\tau}\right)\\
&= \int_0^{+\infty}{\left(\int_{0}^{+\infty}{u(\tau)\,h(t-\tau)d\tau}\right)\, e^{-pt}\, dt}\\
&= \int_0^{+\infty}{\int_{0}^{+\infty}{u(\tau)\,h(t-\tau)}\,e^{-pt}\,dt\,d\tau}\\
&= \int_0^{+\infty}{u(\tau)\int_{0}^{+\infty}{h(t-\tau)\,e^{-p(t-\tau)}}\,dt\,e^{-p\tau}\,d\tau}\\
&= L(h)\int_0^{+\infty}{u(\tau)\,e^{-p\tau}\,d\tau}\\
&= L(h).L(u)\\
\end{align*}
\textbf{Remarque :} La transformée de Laplace est généralement notée au moyen
d'une lettre majuscule. Par exemple, on noterait $L(h) = H$.

\subsection{Propriétés très générales} 

\begin{enumerate}[(i)]
\item Dérivation :
\[
  L(f')(p) = f(0)+pL(f)(p)
\]
\textit{Démonstration :} \quad on sait que
\[
  L(f')(p) = \int_0^{+\infty}{f'(t)\, e^{-pt}dt}
\]
Par intégration par parties on obtient : 
\[ \smallermaths
  L(f')(p) = \left[f(t)e^{-pt}\right]_0^{+\infty}
  -\int_0^{+\infty}{f(t)(-pe^{-pt})dt} = f(0)+pL(f)(p)
\]
On suppose que dans les cas qui nous intéressent
\[
 \lim_{t\rightarrow \infty}{f(t)e^{-pt}} = 0  \, , 
\]
d'où $L(f')(p) = p L(f)(p)$ : ``dériver c'est multiplier par $p$".
\item Intégration :
\[
  L\left(\smallint f\right)(p) = \frac{1}{p}\,  L(f)
\]
\textit{Démonstration :} \quad encore une fois par intégration par parties :
\[ \smallermaths
  L\!\left(\smallint f\right)\!(p) =
  \left[\left(\smallint {f}\right)\!(t)
  \frac{e^{-pt}}{-p}\right]_0^{+\infty}-\int_0^{+\infty}{\!\!f(t)
  \frac{e^{-pt}}{-p}dt} = \frac{1}{p}L(f)(p)
\]
On suppose encore une fois que, dans notre cas,
\[
  \lim_{t\rightarrow \infty}{\int{f(t)}e^{-pt}} = 0 
\]

\item Décalage : Soit $\tau \ge 0$ et $u_{\tau}$ définie par $u_{\tau}(t) =
u(t-\tau)$.\\ D'une part on a, 
\[ \smallermaths
  L\big(u_{\tau}\big)(p) = \int_0^{+\infty}{u_{\tau}(t)\, e^{-pt}\, dt}
  = \int_0^{+\infty}{u(t-\tau)\, e^{-pt}\, dt}
\]
En effectuant le changement de variable $\theta = t - \tau$ on a :
\[
  L\big(u_{\tau}\big)(p) =
  \int_0^{+\infty}{u(\theta)e^{-p(\theta + \tau)}d\theta } =
  e^{-p\tau}\, L(u)(p)
\]
Décaler de $\tau$ revient à multiplier par une exponentielle (ici on devrait
intégrer depuis $-\tau $, mais comme $u$ est nulle sur $\mathbb{R}_-$ on peut
intégrer depuis 0).\\
\\D'autre part, on a $u_{\tau} = u * \delta_{\tau}$ car :
\[
  \big(u * \delta_{\tau}\big)(t) = \int_0^{+\infty}{\delta_{\tau}(x)u(t-x)dx} = u(t-\tau)
\]
Ainsi on a :
\[
  L(u_{\tau}) = L(u * \delta_{\tau}) = L(u).L(\delta_{\tau})
\]
Et donc :
\[
  L\big(\delta_{\tau}\big)(p) = e^{-p\tau}
\]
\end{enumerate}

\subsection{Exemples usuels}
\begin{enumerate}[(i)]
\item Les Diracs : \quad$L(\delta) = 1$ et $L(\delta_{\tau}) = e^{-p\tau }$.

\item Échelons : La représentation d'un échelon est :
\begin{center}
  \includegraphics[width=0.5\linewidth]{fig2014/GJeanne2014s1-2_fig06.pdf}
\end{center}
C'est l'intégrale d'un Dirac. Ainsi,
$L(\indicatrice\nolimits_{\R^+}) = \frac{1}{p}$

\item Fonctions du type : 
\begin{center}
  \includegraphics[width=0.5\linewidth]{fig2014/GJeanne2014s1-2_fig07.pdf}
\end{center}
C'est l'intégrale de la fonction précédente. Ainsi :
$L(t.\indicatrice\nolimits_{\R^+}) = \frac{1}{p^2}$

\item Plus généralement, \quad $L(\frac{t^n}{n!}.\indicatrice_{\mathbb{R}^+}) = \frac{1}{p^n}$

\item Exponentielle : \quad $L(e^{\alpha t}.\indicatrice_{\mathbb{R}^+}) = \frac{1}{p+ \alpha}$

\item Sinus : \quad $L(\sin(\omega t)) = \frac{\omega}{p^2+\omega^2}$

\item Cosinus : \quad $L(\cos(\omega t)) = \frac{p}{p^2+\omega^2}$
\end{enumerate}
Dans tout ce qui précède, $\indicatrice_{\mathbb{R}_+}$ est la fonction
indicatrice de $\mathbb{R}_+$, i.e.\ la fonction qui vaut 1 sur $\mathbb{R}_+$
et qui est nulle autre part.

\section{Applications et outils proches}

\subsection{Résolution d'équations différentielles}

Essayons de trouver les solutions de l'équation :
\[
  \left\{ 
  \begin{array}{l}
  f''(t)+f(t) = \sin (2t)\\
  f(0) = 0\\
  f'(0) = 0\\
  \end{array} \right.
\]
Avec un peu d'habitude, on sait que la solution est de la forme :
\[
  f(t) = (at + b)\sin(2t) + (ct + d)\cos(2t)
\]
Même si on se souvient de la forme de la solution, il faut encore réaliser des
calculs assez pénibles pour arriver à déterminer $a$, $b$, $c$ et $d$. La
transformée de Laplace permet de résoudre une telle équation \emph{sans
connaître la forme de la solution}, et au prix de calculs plus modestes. \\

Comme $f(0) = f'(0) = 0$, en passant à la transformée de Laplace, cette
équation se transforme en :
\[
  p^2F(p) + F(p) = L(\sin(2t))(p) = \frac{2}{p^2+4}
\]
(En notant $F(p) = L(f)(p)$ et d'après l'exemple (vi) de la partie précédente)\\

Ainsi, en isolant $F(p)$,
\[ \smallmaths
  F(p) = \frac{2}{(p^2+4)(p^2+1)} = \frac{2}{(p +2i)(p-2i)(p+i)(p-i)}
\]
Il s'agit alors de décomposer cette fraction en éléments simples. On obtient :
\[
  F(p) = \frac{1}{6}\frac{i}{p+2i}
  -\frac{1}{6}\frac{i}{p-2i}+\frac{1}{3}\frac{i}{p+i}-\frac{1}{3}\frac{i}{p-i}
\]
On repasse alors par l'inverse de la transformée de Laplace et on a :
\[
  f(t) = \frac{1}{6i}e^{-2it}-\frac{1}{6i}e^{2it}+\frac{1}{3i}e^{-it}-\frac{1}{3i}e^{it}
\]
On utilise alors la formule de De Moivre ($\sin(\omega t) = \frac{e^{\omega it}
- e^{-\omega it}}{-i}$) et on obtient :
\[
  f(t) = \frac{1}{3}\sin(2t) + \frac{2}{3}\sin(t)
\]
On a alors assez facilement pu résoudre cette équation. \\

Avec la transformée de Laplace on possède donc une méthode générale qui nous
permet de résoudre plus simplement certaines équations différentielles :
\begin{enumerate}
  \item On transforme l'équation différentielle à l'aide de la transformée de
    Laplace.
  \item On résout l'équation algébrique obtenue, qui est souvent plus simple à
    résoudre.
  \item On utilise la transformée de Laplace inverse pour retrouver les
    solutions.
\end{enumerate}


\subsection{Transformée en Z}

\subsubsection{Convolution dans $\mathbb{Z}$}

En introduction, pour définir la convolution, nous étions partis d'un exemple en
temps discret, puis étions passé en continu en faisant tendre le pas de temps
vers 0. Il est cependant tout à fait possible de rester en temps discret, et de
définir la convolution, qui s'appliquera alors à des suites.\\

Soient $u$ et $v$ deux fonctions de $\mathbb{Z}$ dans $\mathbb{C}$, on
définit $u \ast v$, la convolution de $u$ et $v$, comme :
\[
  \forall n \in \mathbb{Z}, \: \big(u*v\big)[n]
  = \sum_{k=-\infty}^{+\infty}{u[k]\, v[n-k]}
\]
quand cette somme converge.\\

Cette convolution a les mêmes propriétés que celles décrites dans le cas
continu, et on peut appliquer exactement le même raisonnement qu'avant et se
demander s'il existe un analogue de la transformée de Laplace dans le cas
discret, qui pourrait transformer convolution en produit.  Cet analogue existe
bel et bien, et se nomme la transformée en Z.

\subsubsection{Définition de la transformée en Z}

On appelle transformée en Z de la suite $u$ la fonction $Z(u)$, de
variable complexe $z$, telle que :
\[
  Z(u)(z) = \sum_{n=0}^{+\infty}{u[n]\, z^{-n}}
\]
Encore une fois, on part de 0 et non de $-\infty$ dans l'expression car le
point de vue adopté considère des suites nulles en dessous d'un certain rang.

\subsubsection{Quelques propriétés}

La transformée en Z possède des propriétés analogues à celles de la transformée
de Laplace, parmi celles-ci on trouve (sans démonstration) :
\begin{enumerate}[(i)]
\item Linéarité : \quad $Z(u+v) = Z(u) + Z(v)$
\item Décalage : soit $k \ge 0$. Alors,\smallformatnewline
$\forall z \in \mathbb{C}$,  \quad
$Z(u[\, \cdot\, -k])(z) = z^{-k}\,
Z(u[\, \cdot\, ])(z)$
\item Convolution : \quad $Z(u*v) = Z(u)\times Z(v)$
\end{enumerate}

\subsubsection{Utilisation}

On a vu que la transformée de Laplace pouvait se révéler très utile pour
résoudre certaines équations différentielles. De la même manière, la
transformée en Z peut permettre de résoudre bien plus facilement certaines
équations de récurrence. En appliquant la propriété de décalage au dessus, on
peut transformer une équation de récurrence en une autre équation qui peut
s'avérer plus simple à résoudre, puis prendre la transformée en Z inverse.

\subsection{Lien avec les probabilités}

Jusqu'à maintenant, nous avons introduit toutes les notions dans le contexte
des systèmes dynamiques, et de l'automatique. Cependant, ces notions sont aussi
très présentes dans le domaine des probabilités.

\subsubsection{Convolution et probabilité}

En probabilité, la convolution permet de donner la loi d'une somme de variables
aléatoires réelles à densité. Ce résultat est au programme de BCSPT, et c'est
sûrement pour cette application que vous avez déjà vu la convolution ! Plus
formellement, soit $X$ une variable aléatoire réelle, i.e.\ une fonction de
$(\Omega , \mathcal{F} , \mathbb{P} )$, un espace de probabilité, dans
$\mathbb{R}$. On dit qu'elle admet une densité $f$ si, pour tout $x \in
\mathbb{R}$ on a :
\[
  \mathbb{P}(X \le x) = \int_{-\infty}^{x}{f(t)dt}
\]

Prenons maintenant deux variables aléatoires réelles indépendantes, $X$ et $Y$,
de densités $f$ et $g$ respectivement. Alors $X + Y$ admet une densité qui est
$h := f*g$. Démontrons le :\\

En reprenant les notations au-dessus, notons $V = X + Y$ et $U = (X, Y)$. Comme
$X$ et $Y$ sont indépendantes, $U$ admet une densité $u:(x,y)\mapsto f(x)g(y)$.
Calculons $\mathbb{E}[\phi (V)]$ pour $\phi$ une fonction
quelconque\footnote{Une caractérisation équivalente du fait qu'une variable
aléatoire réelle $V$ admet une densité $h$ est que, pour toute fonction
\emph{continue bornée} $\phi$, $\Esp\left(\phi(V)\right) = \int_{\R}
\phi(x)\, h(x)\, dx$. On pourrait utiliser cette propriété pour définir ce
qu'est une variable à densité, mais ici on va utiliser la définition donnée dans
le texte. On considère donc pour l'instant une fonction $\phi$ quelconque telle
que l'intégrale a un sens.}. Par
transfert :
\[
  \mathbb{E}[\phi (V)] = \int_{\mathbb{R}^2}{\phi(x+y)\,  f(x)\, g(y)\, dx\, dy}
\]
En faisant le changement de variable :
\[
\left\{
\begin{array}{l c l}
  u &=& x + y\\
  v &=& x\\
\end{array} \right.
\]
On obtient :
\[
  \mathbb{E}[\phi (V)] = \int_{\mathbb{R}^2}{\phi (u)\, f(v)\, g(u-v)\, du\, dv}
\]
Et par Fubini :
\[
  \mathbb{E}[\phi (V)] = \int_{\mathbb{R}}{\phi (u) 
  \left( \int_{\mathbb{R}}{f(v)g(u-v)dv}\right) du}
\]
Comme on a choisi $\phi$ quelconque, cela prouve que
$\int_{\mathbb{R}}{f(v)\, g(u-v)\, dv}$ est la densité de $V$, c'est-à-dire que
la densité de $X+Y$ est $f*g$. Plus précisemment, pour se ramener à la
définition de la densité donnée au-dessus, il suffit de prendre $\phi =
\indicatrice_{]-\infty ,x[}$. Dans ce cas, $\mathbb{E}[\phi (V)] = \mathbb{P}(V
\le x) = \int_{-\infty}^{x}{\big(f*g\big)(t)dt}$ et on a bien le résultat
voulu.

\subsubsection{Détermination des moments d'une loi}

La transformée de Laplace permet de trouver de manière systématique et assez
efficace les différents moments d'une loi positive. Pour rappel, on dit qu'une
variable aléatoire $X$ admet un moment d'ordre $n$ si
$\mathbb{E}\big[|X|^n\big]$ est finie. On voit qu'avoir un moment d'ordre 1
revient à avoir une espérance, et un moment d'ordre 2 une variance.\\

Lorsqu'une variable aléatoire positive $X$ admet une densité $f$, on peut
prendre la transformée de Laplace de cette densité :
\[
  L(f)(p) = \int_0^{+\infty} f(x)\,  e^{-px} \, dt = \Esp\left[e^{-pX}\right]\,.
\]
On reconnaît alors la fonction génératrice des moments de $X$. Plus précisément,
selon la convention usuelle, la fonction génératrice des moments est donnée,
lorsqu'elle existe, par
\[
  M_X(t) = \Esp\left[e^{tX}\right], \quad t \in \R
\]
Ainsi,
\[
  \forall t \in \R, \quad M_X(t) = L(f)(-t)\, .
\]

Justifions maintenant l'appellation ``fonction génératrice des moments". Sans
détailler ce qu'on appelle une fonction génératrice, montrons que l'on peut
retrouver les moments de $X$ à partir de $M_X$:\\

Si $X$ admet un moment d'ordre 1, on admet qu'on peut alors dériver $M_X$ par
rapport à $t$, et qu'on peut dériver sous l'espérance, ce qui donne :
\[
  \frac{d}{dt}M_X(t) = \mathbb{E}\left[X e^{t X}\right]
\]
En prenant cette dérivée en 0, on obtient :
\[
  \frac{d}{dt}M_X(0) = \mathbb{E}\left[X\right]
\]
On peut donc retrouver l'espérance de $X$ en dérivant sa fonction génératrice
des moments en 0. Plus généralement, si $X$ admet un moment d'ordre $n$, elle
est dérivable $n$ fois en 0, et on a :
\[
  \frac{d^n}{dt^n} M_X(0) = \mathbb{E}\left[X^n\right]
\]
Une fois la fonction génératrice des moments déterminée, il est généralement
assez simple de la dériver plusieurs fois. Plutôt que de calculer directement
de façon séparée l'espérance et la variance d'une loi, il peut être plus simple
d'en prendre la transformée de Laplace et de la dériver 2 fois.

\subsection{Lien avec Fourier}

La transformée de Laplace a un lien très fort avec un autre outil central
des mathématiques : la transformée de Fourier. On définit la transformée
de Fourier de $f$, notée $\widehat{f}$, par
\[
  \widehat{f}(x) = \int_{-\infty}^{+\infty} f(t) \,
  e^{-{\boldsymbol i} x t}\, dt\, .
\]
On voit donc qu'il existe deux différences entre la transformée de Fourier et
la transformée de Laplace :
\begin{enumerate}
  \item Avec la transformée de Laplace, on n'intègre que sur $\R^+$, alors qu'on
  intègre sur tout $\R$ avec la transformée de Fourier. Néanmoins, ce n'est
  pas une différence majeure, et la \emph{transformée de Laplace bilatérale},
  avec laquelle on intègre sur tout $\R$, existe également.
  \item Avec la transformée de Fourier, on se limite aux complexes de la forme
  $p = {\boldsymbol i} t$. En ce sens, la transformée de Laplace (bilatérale)
  généralise la transformée de Fourier.
\end{enumerate}
Ce qu'il faut retenir, c'est que transformée de Laplace et transformée de
Fourier sont deux outils très proches. Le choix de l'un ou l'autre dépendra
surtout du problème précis ou du domaine d'étude : par exemple, en probabilité,
la transformée de Fourier est très utilisée (la transformée de Fourier de la
densité est ce qu'on appelle la fonction caractéristique) alors que la
transformée de Laplace l'est beaucoup moins.
