==========================
GT MATHS-BIO LECTURE NOTES
==========================

This folder contains the LaTeX source code for the lectures notes of the
GT maths-bio of the École normale supérieure. The compiled pdfs are to be
found at http://www.gt-mathsbio.biologie.ens.fr/notes

The files are named ASmith2000sx(-y)_what, where :
  - A. Smith is the person who animated the lesson
  - 2000 is the year of the lesson
  - sx(-y) is the number of lessons on the subject
    covered by the notes, e.g. s1, s1-3, etc
  - what indicates what's in the file :
    * A4 : LaTeX file with preamble adapted to A4 paper
    * ebook : LaTeX file with preamble adapted to e-readers
    * content : the actual content of the file

The figures are in the 'fig' folders and are named following a similar
convention. There is one BibTeX bibliography per year.

This structure is there to factorize the code in case we want to compile
to other formats, or concatenate the notes in a big document.

Copyright © 2013-2016 GT maths-bio <gt-mathsbio.contact@lists.ens.fr>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
